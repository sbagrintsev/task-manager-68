package ru.tsc.bagrintsev.tm.listener.user;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.request.user.UserSignOutRequest;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.event.ConsoleEvent;

@Component
public final class UserSignOutListener extends AbstractUserListener {

    @NotNull
    @Override
    public String description() {
        return "Sign user out.";
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@userSignOutListener.name() == #consoleEvent.name")
    public void handle(@NotNull ConsoleEvent consoleEvent) {
        showOperationInfo();
        authEndpoint.signOut(new UserSignOutRequest(getToken()));
    }

    @NotNull
    @Override
    public String name() {
        return "sign-out";
    }

}
