package ru.tsc.bagrintsev.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.tsc.bagrintsev.tm.api.sevice.ITokenService;

@Getter
@Setter
@Service
public class TokenService implements ITokenService {

    @NotNull
    private String token;

}
