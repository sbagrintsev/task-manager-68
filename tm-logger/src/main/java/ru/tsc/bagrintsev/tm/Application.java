package ru.tsc.bagrintsev.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.tsc.bagrintsev.tm.component.Bootstrap;
import ru.tsc.bagrintsev.tm.configuration.LoggerConfiguration;

/**
 * @author Sergey Bagrintsev
 */

public final class Application {

    public static void main(@Nullable final String[] args) {
        try (@NotNull final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(LoggerConfiguration.class)) {
            @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
            context.registerShutdownHook();
            bootstrap.run();
        }
    }

}
