package ru.tsc.bagrintsev.tm.controller;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.tsc.bagrintsev.tm.dto.FormDTO;
import ru.tsc.bagrintsev.tm.model.Project;
import ru.tsc.bagrintsev.tm.model.Task;
import ru.tsc.bagrintsev.tm.service.ProjectService;
import ru.tsc.bagrintsev.tm.service.TaskService;

@Controller
@RequiredArgsConstructor
@RequestMapping("/task/")
public class TaskController {

    private final TaskService taskService;

    private final ProjectService projectService;

    @PostMapping("create")
    public String create(@ModelAttribute("task") final FormDTO form) {
        @NotNull final Task task = form.getTask();
        @NotNull final Project project = projectService.findById(form.getText());
        task.setProject(project);
        taskService.save(task);
        return "redirect:" + form.getReferer();
    }

    @GetMapping("create")
    public ModelAndView create() {
        final Task task = new Task();
        final FormDTO form = new FormDTO();
        form.setTask(task);
        form.setReferer("/tasks");
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-edit");
        modelAndView.addObject("form", form);
        modelAndView.addObject("projects", projectService.findAll());
        return modelAndView;
    }

    @GetMapping("delete/{id}")
    public String delete(@PathVariable("id") String id) {
        taskService.deleteById(id);
        return "redirect:/tasks";
    }

    @PostMapping("edit/{id}")
    public String edit(@ModelAttribute("task") final FormDTO form) {
        @NotNull final Task task = form.getTask();
        @NotNull final Project project = projectService.findById(form.getText());
        task.setProject(project);
        taskService.save(task);
        return "redirect:" + form.getReferer();
    }

    @GetMapping("edit/{id}")
    public ModelAndView edit(
            @RequestHeader String referer,
            @PathVariable("id") String id
    ) {
        final Task task = taskService.findById(id);
        final FormDTO form = new FormDTO();
        form.setTask(task);
        form.setReferer(referer);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-edit");
        modelAndView.addObject("form", form);
        modelAndView.addObject("projects", projectService.findAll());
        return modelAndView;
    }

}
