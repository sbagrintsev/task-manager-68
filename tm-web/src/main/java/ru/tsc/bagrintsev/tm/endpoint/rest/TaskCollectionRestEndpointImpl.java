package ru.tsc.bagrintsev.tm.endpoint.rest;

import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.bagrintsev.tm.api.endpoint.rest.TaskCollectionRestEndpoint;
import ru.tsc.bagrintsev.tm.model.Task;
import ru.tsc.bagrintsev.tm.service.TaskService;

import java.util.List;

@RestController
@NoArgsConstructor
@RequestMapping("/api/tasks")
//@WebService(endpointInterface = "ru.tsc.bagrintsev.tm.api.endpoint.rest.TaskCollectionEndpoint")
public class TaskCollectionRestEndpointImpl implements TaskCollectionRestEndpoint {

    private TaskService taskService;

    //    @WebMethod
    @DeleteMapping
    public void delete(
            @RequestBody
//            @WebParam(name = "tasks")
            List<String> taskIds
    ) {
        taskService.deleteAll(taskIds);
    }

    //    @WebMethod
    @GetMapping
    public List<Task> get() {
        return taskService.findAll();
    }

    //    @WebMethod
    @PostMapping
    public void save(
            @RequestBody
//            @WebParam(name = "tasks")
            List<Task> tasks
    ) {
        taskService.saveAll(tasks);
    }

    @Autowired
    public void setTaskService(TaskService taskService) {
        this.taskService = taskService;
    }

}
