package ru.tsc.bagrintsev.tm.endpoint.rest;

import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.bagrintsev.tm.api.endpoint.rest.ProjectRestCollectionEndpoint;
import ru.tsc.bagrintsev.tm.model.Project;
import ru.tsc.bagrintsev.tm.service.ProjectService;

import java.util.List;

@RestController
@NoArgsConstructor
@RequestMapping("/api/projects")
//@WebService(endpointInterface = "ru.tsc.bagrintsev.tm.api.endpoint.rest.ProjectCollectionEndpoint")
public class ProjectCollectionRestEndpointImpl implements ProjectRestCollectionEndpoint {

    private ProjectService projectService;

    //    @WebMethod
    @DeleteMapping
    public void delete(
            @RequestBody
//            @WebParam(name = "projects")
            List<String> projectIds
    ) {
        projectService.deleteAll(projectIds);
    }

    //    @WebMethod
    @GetMapping
    public List<Project> get() {
        return projectService.findAll();
    }

    //    @WebMethod
    @PostMapping
    public void save(
            @RequestBody
//            @WebParam(name = "projects")
            List<Project> projects
    ) {
        projectService.saveAll(projects);
    }

    @Autowired
    public void setProjectService(ProjectService projectService) {
        this.projectService = projectService;
    }

}
