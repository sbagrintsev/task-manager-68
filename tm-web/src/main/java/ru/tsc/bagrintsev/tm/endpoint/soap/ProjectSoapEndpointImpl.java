package ru.tsc.bagrintsev.tm.endpoint.soap;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.tsc.bagrintsev.tm.dto.soap.*;
import ru.tsc.bagrintsev.tm.service.ProjectService;

@Endpoint
public class ProjectSoapEndpointImpl {

    public static final String NAME = "project";

    public static final String PORT_TYPE_NAME = "ProjectEndpoint";

    public static final String NAMESPACE = "http://bagrintsev.tsc.ru/tm/dto/soap";

    private ProjectService projectService;

    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteRequest", namespace = NAMESPACE)
    public ProjectDeleteResponse delete(@RequestPayload final ProjectDeleteRequest request) {
        projectService.deleteById(request.getId());
        return new ProjectDeleteResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectGetRequest", namespace = NAMESPACE)
    public ProjectGetResponse get(@RequestPayload final ProjectGetRequest request) {
        @NotNull final ProjectGetResponse response = new ProjectGetResponse();
        response.setProject(projectService.findById(request.getId()));
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectSaveRequest", namespace = NAMESPACE)
    public ProjectSaveResponse save(@RequestPayload final ProjectSaveRequest request) {
        projectService.save(request.getProject());
        return new ProjectSaveResponse();
    }

    @Autowired
    public void setProjectService(ProjectService projectService) {
        this.projectService = projectService;
    }

}
