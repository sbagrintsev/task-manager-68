package ru.tsc.bagrintsev.tm.configuration;

import lombok.NoArgsConstructor;
import org.hibernate.cfg.AvailableSettings;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;
import ru.tsc.bagrintsev.tm.endpoint.soap.ProjectCollectionSoapEndpointImpl;
import ru.tsc.bagrintsev.tm.endpoint.soap.ProjectSoapEndpointImpl;
import ru.tsc.bagrintsev.tm.endpoint.soap.TaskCollectionSoapEndpointImpl;
import ru.tsc.bagrintsev.tm.endpoint.soap.TaskSoapEndpointImpl;
import ru.tsc.bagrintsev.tm.service.PropertyService;

import javax.sql.DataSource;
import java.util.Properties;

@EnableWs
@Configuration
@NoArgsConstructor
@EnableTransactionManagement
@ComponentScan("ru.tsc.bagrintsev.tm")
@EnableJpaRepositories("ru.tsc.bagrintsev.tm.api.repository")
public class ApplicationConfiguration extends WsConfigurerAdapter {

    @NotNull
    public final static String ROOT = "/ws/";

    @NotNull
    public final static String LOCATION = "http://localhost:8080";


    @Nullable
    private PropertyService propertyService;

    private Properties additionalProperties() {
        Properties properties = new Properties();
        assert propertyService != null;
        properties.setProperty(AvailableSettings.DIALECT, propertyService.getDatabaseSqlDialect());
        properties.setProperty(AvailableSettings.HBM2DDL_AUTO, propertyService.getDatabaseHbm2DdlAuto());
        properties.setProperty(AvailableSettings.SHOW_SQL, propertyService.getDatabaseShowSql());
        return properties;
    }

    @Bean
    @NotNull
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(propertyService.getDatabaseDriver());
        dataSource.setUsername(propertyService.getDatabaseUserName());
        dataSource.setPassword(propertyService.getDatabaseUserPassword());
        dataSource.setUrl(propertyService.getDatabaseUrl());
        return dataSource;
    }

    @Bean
    @NotNull
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        @NotNull final LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource());
        factoryBean.setPackagesToScan("ru.tsc.bagrintsev.tm.model");
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setJpaProperties(additionalProperties());
        return factoryBean;
    }

    @Bean
    @NotNull
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
        return new PersistenceExceptionTranslationPostProcessor();
    }

    @Autowired
    public void setPropertyService(@NotNull final PropertyService propertyService) {
        this.propertyService = propertyService;
    }

    @Bean
    @NotNull
    public PlatformTransactionManager transactionManager() {
        @NotNull final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
        return transactionManager;
    }

    @Bean(name = ProjectSoapEndpointImpl.NAME)
    public DefaultWsdl11Definition projectWsdl11Definition(XsdSchema projectEndpointSchema) {
        final DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName(ProjectSoapEndpointImpl.PORT_TYPE_NAME);
        wsdl11Definition.setLocationUri(LOCATION + ROOT + ProjectSoapEndpointImpl.NAME);
        wsdl11Definition.setTargetNamespace(ProjectSoapEndpointImpl.NAMESPACE);
        wsdl11Definition.setSchema(projectEndpointSchema);
        return wsdl11Definition;
    }

    @Bean
    public XsdSchema projectEndpointSchema() {
        return new SimpleXsdSchema(new ClassPathResource("xsd/projectEndpoint.xsd"));
    }

    @Bean(name = ProjectCollectionSoapEndpointImpl.NAME)
    public DefaultWsdl11Definition projectCollectionWsdl11Definition(XsdSchema projectCollectionEndpointSchema) {
        final DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName(ProjectCollectionSoapEndpointImpl.PORT_TYPE_NAME);
        wsdl11Definition.setLocationUri(LOCATION + ROOT + ProjectCollectionSoapEndpointImpl.NAME);
        wsdl11Definition.setTargetNamespace(ProjectCollectionSoapEndpointImpl.NAMESPACE);
        wsdl11Definition.setSchema(projectCollectionEndpointSchema);
        return wsdl11Definition;
    }

    @Bean
    public XsdSchema projectCollectionEndpointSchema() {
        return new SimpleXsdSchema(new ClassPathResource("xsd/projectCollectionEndpoint.xsd"));
    }

    @Bean(name = TaskSoapEndpointImpl.NAME)
    public DefaultWsdl11Definition taskWsdl11Definition(XsdSchema taskEndpointSchema) {
        final DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName(TaskSoapEndpointImpl.PORT_TYPE_NAME);
        wsdl11Definition.setLocationUri(LOCATION + ROOT + TaskSoapEndpointImpl.NAME);
        wsdl11Definition.setTargetNamespace(TaskSoapEndpointImpl.NAMESPACE);
        wsdl11Definition.setSchema(taskEndpointSchema);
        return wsdl11Definition;
    }

    @Bean
    public XsdSchema taskEndpointSchema() {
        return new SimpleXsdSchema(new ClassPathResource("xsd/taskEndpoint.xsd"));
    }

    @Bean(name = TaskCollectionSoapEndpointImpl.NAME)
    public DefaultWsdl11Definition taskCollectionWsdl11Definition(XsdSchema taskCollectionEndpointSchema) {
        final DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName(TaskCollectionSoapEndpointImpl.PORT_TYPE_NAME);
        wsdl11Definition.setLocationUri(LOCATION + ROOT + TaskCollectionSoapEndpointImpl.NAME);
        wsdl11Definition.setTargetNamespace(TaskCollectionSoapEndpointImpl.NAMESPACE);
        wsdl11Definition.setSchema(taskCollectionEndpointSchema);
        return wsdl11Definition;
    }

    @Bean
    public XsdSchema taskCollectionEndpointSchema() {
        return new SimpleXsdSchema(new ClassPathResource("xsd/taskCollectionEndpoint.xsd"));
    }

}
