package ru.tsc.bagrintsev.tm.api.endpoint.rest;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.tsc.bagrintsev.tm.model.Task;

import java.util.List;

//@WebService
public interface TaskCollectionRestEndpoint {

    //    @WebMethod
    @DeleteMapping(produces = "application/json")
    void delete(@RequestBody
//                @WebParam(name = "tasks")
                List<String> taskIds
    );

    //    @WebMethod
    @GetMapping(produces = "application/json")
    List<Task> get();

    //    @WebMethod
    @PostMapping(produces = "application/json")
    void save(@RequestBody
//              @WebParam(name = "tasks")
              List<Task> tasks
    );

}
