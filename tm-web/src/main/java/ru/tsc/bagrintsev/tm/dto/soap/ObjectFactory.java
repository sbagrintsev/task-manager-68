//
// This file was generated by the Eclipse Implementation of JAXB, v3.0.0 
// See https://eclipse-ee4j.github.io/jaxb-ri 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2022.06.09 at 04:59:48 PM MSK 
//


package ru.tsc.bagrintsev.tm.dto.soap;

import ru.tsc.bagrintsev.tm.model.Project;
import ru.tsc.bagrintsev.tm.model.Task;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.tsc.bagrintsev.tm.dto.soap package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.tsc.bagrintsev.tm.dto.soap
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ProjectCollectionDeleteRequest }
     * 
     */
    public ProjectCollectionDeleteRequest createProjectCollectionDeleteRequest() {
        return new ProjectCollectionDeleteRequest();
    }

    /**
     * Create an instance of {@link ProjectCollectionDeleteResponse }
     * 
     */
    public ProjectCollectionDeleteResponse createProjectCollectionDeleteResponse() {
        return new ProjectCollectionDeleteResponse();
    }

    /**
     * Create an instance of {@link ProjectCollectionGetRequest }
     * 
     */
    public ProjectCollectionGetRequest createProjectCollectionGetRequest() {
        return new ProjectCollectionGetRequest();
    }

    /**
     * Create an instance of {@link ProjectCollectionGetResponse }
     * 
     */
    public ProjectCollectionGetResponse createProjectCollectionGetResponse() {
        return new ProjectCollectionGetResponse();
    }

    /**
     * Create an instance of {@link Project }
     * 
     */
    public Project createProject() {
        return new Project();
    }

    /**
     * Create an instance of {@link ProjectCollectionSaveRequest }
     * 
     */
    public ProjectCollectionSaveRequest createProjectCollectionSaveRequest() {
        return new ProjectCollectionSaveRequest();
    }

    /**
     * Create an instance of {@link ProjectCollectionSaveResponse }
     * 
     */
    public ProjectCollectionSaveResponse createProjectCollectionSaveResponse() {
        return new ProjectCollectionSaveResponse();
    }

    /**
     * Create an instance of {@link ProjectDeleteRequest }
     * 
     */
    public ProjectDeleteRequest createProjectDeleteRequest() {
        return new ProjectDeleteRequest();
    }

    /**
     * Create an instance of {@link ProjectDeleteResponse }
     * 
     */
    public ProjectDeleteResponse createProjectDeleteResponse() {
        return new ProjectDeleteResponse();
    }

    /**
     * Create an instance of {@link ProjectGetRequest }
     * 
     */
    public ProjectGetRequest createProjectGetRequest() {
        return new ProjectGetRequest();
    }

    /**
     * Create an instance of {@link ProjectGetResponse }
     * 
     */
    public ProjectGetResponse createProjectGetResponse() {
        return new ProjectGetResponse();
    }

    /**
     * Create an instance of {@link ProjectSaveRequest }
     * 
     */
    public ProjectSaveRequest createProjectSaveRequest() {
        return new ProjectSaveRequest();
    }

    /**
     * Create an instance of {@link ProjectSaveResponse }
     * 
     */
    public ProjectSaveResponse createProjectSaveResponse() {
        return new ProjectSaveResponse();
    }

    /**
     * Create an instance of {@link TaskCollectionDeleteRequest }
     * 
     */
    public TaskCollectionDeleteRequest createTaskCollectionDeleteRequest() {
        return new TaskCollectionDeleteRequest();
    }

    /**
     * Create an instance of {@link TaskCollectionDeleteResponse }
     * 
     */
    public TaskCollectionDeleteResponse createTaskCollectionDeleteResponse() {
        return new TaskCollectionDeleteResponse();
    }

    /**
     * Create an instance of {@link TaskCollectionGetRequest }
     * 
     */
    public TaskCollectionGetRequest createTaskCollectionGetRequest() {
        return new TaskCollectionGetRequest();
    }

    /**
     * Create an instance of {@link TaskCollectionGetResponse }
     * 
     */
    public TaskCollectionGetResponse createTaskCollectionGetResponse() {
        return new TaskCollectionGetResponse();
    }

    /**
     * Create an instance of {@link Task }
     * 
     */
    public Task createTask() {
        return new Task();
    }

    /**
     * Create an instance of {@link TaskCollectionSaveRequest }
     * 
     */
    public TaskCollectionSaveRequest createTaskCollectionSaveRequest() {
        return new TaskCollectionSaveRequest();
    }

    /**
     * Create an instance of {@link TaskCollectionSaveResponse }
     * 
     */
    public TaskCollectionSaveResponse createTaskCollectionSaveResponse() {
        return new TaskCollectionSaveResponse();
    }

    /**
     * Create an instance of {@link TaskDeleteRequest }
     * 
     */
    public TaskDeleteRequest createTaskDeleteRequest() {
        return new TaskDeleteRequest();
    }

    /**
     * Create an instance of {@link TaskDeleteResponse }
     * 
     */
    public TaskDeleteResponse createTaskDeleteResponse() {
        return new TaskDeleteResponse();
    }

    /**
     * Create an instance of {@link TaskGetRequest }
     * 
     */
    public TaskGetRequest createTaskGetRequest() {
        return new TaskGetRequest();
    }

    /**
     * Create an instance of {@link TaskGetResponse }
     * 
     */
    public TaskGetResponse createTaskGetResponse() {
        return new TaskGetResponse();
    }

    /**
     * Create an instance of {@link TaskSaveRequest }
     * 
     */
    public TaskSaveRequest createTaskSaveRequest() {
        return new TaskSaveRequest();
    }

    /**
     * Create an instance of {@link TaskSaveResponse }
     * 
     */
    public TaskSaveResponse createTaskSaveResponse() {
        return new TaskSaveResponse();
    }

}
