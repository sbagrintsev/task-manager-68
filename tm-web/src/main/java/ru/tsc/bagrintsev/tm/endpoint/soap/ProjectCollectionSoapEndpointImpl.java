package ru.tsc.bagrintsev.tm.endpoint.soap;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.tsc.bagrintsev.tm.dto.soap.*;
import ru.tsc.bagrintsev.tm.service.ProjectService;

@Endpoint
public class ProjectCollectionSoapEndpointImpl {

    public static final String NAME = "projectCollection";

    public static final String PORT_TYPE_NAME = "ProjectCollectionEndpoint";

    public static final String NAMESPACE = "http://bagrintsev.tsc.ru/tm/dto/soap";

    private ProjectService projectService;

    @ResponsePayload
    @PayloadRoot(localPart = "projectCollectionDeleteRequest", namespace = NAMESPACE)
    public ProjectCollectionDeleteResponse delete(@RequestPayload final ProjectCollectionDeleteRequest request) {
        projectService.deleteAll(request.getProjects());
        return new ProjectCollectionDeleteResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectCollectionGetRequest", namespace = NAMESPACE)
    public ProjectCollectionGetResponse get(@RequestPayload final ProjectCollectionGetRequest request) {
        @NotNull final ProjectCollectionGetResponse response = new ProjectCollectionGetResponse();
        response.setProjects(projectService.findAll());
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectCollectionSaveRequest", namespace = NAMESPACE)
    public ProjectCollectionSaveResponse save(@RequestPayload final ProjectCollectionSaveRequest request) {
        projectService.saveAll(request.getProjects());
        return new ProjectCollectionSaveResponse();
    }

    @Autowired
    public void setProjectService(ProjectService projectService) {
        this.projectService = projectService;
    }

}
