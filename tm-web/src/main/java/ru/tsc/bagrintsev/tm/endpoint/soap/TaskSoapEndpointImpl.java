package ru.tsc.bagrintsev.tm.endpoint.soap;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.tsc.bagrintsev.tm.dto.soap.*;
import ru.tsc.bagrintsev.tm.service.TaskService;

@Endpoint
public class TaskSoapEndpointImpl {

    public static final String NAME = "task";

    public static final String PORT_TYPE_NAME = "TaskEndpoint";

    public static final String NAMESPACE = "http://bagrintsev.tsc.ru/tm/dto/soap";

    private TaskService taskService;

    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteRequest", namespace = NAMESPACE)
    public TaskDeleteResponse delete(@RequestPayload final TaskDeleteRequest request) {
        taskService.deleteById(request.getId());
        return new TaskDeleteResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskGetRequest", namespace = NAMESPACE)
    public TaskGetResponse get(@RequestPayload final TaskGetRequest request) {
        @NotNull final TaskGetResponse response = new TaskGetResponse();
        response.setTask(taskService.findById(request.getId()));
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskSaveRequest", namespace = NAMESPACE)
    public TaskSaveResponse save(@RequestPayload final TaskSaveRequest request) {
        taskService.save(request.getTask());
        return new TaskSaveResponse();
    }

    @Autowired
    public void setTaskService(TaskService taskService) {
        this.taskService = taskService;
    }

}
