package ru.tsc.bagrintsev.tm.dto;

import lombok.Getter;
import lombok.Setter;
import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.model.Project;
import ru.tsc.bagrintsev.tm.model.Task;

@Getter
@Setter
public class FormDTO {

    private final Status[] statuses = Status.values();

    private String referer;

    private Project project;

    private Task task;

    private String text;

}
