package ru.tsc.bagrintsev.tm.controller;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.tsc.bagrintsev.tm.model.Project;
import ru.tsc.bagrintsev.tm.service.ProjectService;
import ru.tsc.bagrintsev.tm.service.TaskService;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Controller
@RequiredArgsConstructor
@RequestMapping("/tasks")
public class TaskCollectionController {

    private final TaskService taskService;

    private final ProjectService projectService;

    @GetMapping
    public ModelAndView get() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-list");
        modelAndView.addObject("tasks", taskService.findAll());
        @NotNull final Map<String, Project> projects =
                projectService
                        .findAll()
                        .stream()
                        .collect(Collectors.toMap(Project::getId, Function.identity()));
        modelAndView.addObject("projects", projects);
        return modelAndView;
    }

}
