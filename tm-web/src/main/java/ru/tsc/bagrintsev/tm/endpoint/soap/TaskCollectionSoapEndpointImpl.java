package ru.tsc.bagrintsev.tm.endpoint.soap;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.tsc.bagrintsev.tm.dto.soap.*;
import ru.tsc.bagrintsev.tm.service.TaskService;

@Endpoint
public class TaskCollectionSoapEndpointImpl {

    public static final String NAME = "taskCollection";

    public static final String PORT_TYPE_NAME = "TaskCollectionEndpoint";

    public static final String NAMESPACE = "http://bagrintsev.tsc.ru/tm/dto/soap";

    private TaskService taskService;

    @ResponsePayload
    @PayloadRoot(localPart = "taskCollectionDeleteRequest", namespace = NAMESPACE)
    public TaskCollectionDeleteResponse delete(@RequestPayload final TaskCollectionDeleteRequest request) {
        taskService.deleteAll(request.getTasks());
        return new TaskCollectionDeleteResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskCollectionGetRequest", namespace = NAMESPACE)
    public TaskCollectionGetResponse get(@RequestPayload final TaskCollectionGetRequest request) {
        @NotNull final TaskCollectionGetResponse response = new TaskCollectionGetResponse();
        response.setTasks(taskService.findAll());
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskCollectionSaveRequest", namespace = NAMESPACE)
    public TaskCollectionSaveResponse save(@RequestPayload final TaskCollectionSaveRequest request) {
        taskService.saveAll(request.getTasks());
        return new TaskCollectionSaveResponse();
    }

    @Autowired
    public void setTaskService(TaskService taskService) {
        this.taskService = taskService;
    }

}

