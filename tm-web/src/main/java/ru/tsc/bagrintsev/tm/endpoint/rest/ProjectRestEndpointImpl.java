package ru.tsc.bagrintsev.tm.endpoint.rest;

import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.bagrintsev.tm.api.endpoint.rest.ProjectRestEndpoint;
import ru.tsc.bagrintsev.tm.model.Project;
import ru.tsc.bagrintsev.tm.service.ProjectService;

//import javax.jws.WebMethod;
//import javax.jws.WebParam;
//import javax.jws.WebService;

@RestController
@NoArgsConstructor
@RequestMapping("/api/project")
//@WebService(endpointInterface = "ru.tsc.bagrintsev.tm.api.endpoint.rest.ProjectEndpoint")
public class ProjectRestEndpointImpl implements ProjectRestEndpoint {

    private ProjectService projectService;

    //    @WebMethod
    @DeleteMapping("/{id}")
    public void delete(
            @PathVariable("id")
//            @WebParam(name = "id")
            String id
    ) {
        projectService.deleteById(id);
    }

    //    @WebMethod
    @GetMapping("/{id}")
    public Project get(
            @PathVariable("id")
//            @WebParam(name = "id")
            String id
    ) {
        return projectService.findById(id);
    }

    //    @WebMethod
    @PostMapping
    public void save(
            @RequestBody
//            @WebParam(name = "project")
            Project project
    ) {
        projectService.save(project);
    }

    @Autowired
    public void setProjectService(ProjectService projectService) {
        this.projectService = projectService;
    }

}
