package ru.tsc.bagrintsev.tm.api.endpoint.rest;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.tsc.bagrintsev.tm.model.Project;

import java.util.List;

//@WebService
public interface ProjectRestCollectionEndpoint {

    //    @WebMethod
    @DeleteMapping(produces = "application/json")
    void delete(@RequestBody
//                @WebParam(name = "projects")
                List<String> projectIds
    );

    //    @WebMethod
    @GetMapping(produces = "application/json")
    List<Project> get();

    //    @WebMethod
    @PostMapping(produces = "application/json")
    void save(@RequestBody
//              @WebParam(name = "projects")
              List<Project> projects
    );

}
