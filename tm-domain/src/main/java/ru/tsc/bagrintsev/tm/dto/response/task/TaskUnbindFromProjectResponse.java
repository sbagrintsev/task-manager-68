package ru.tsc.bagrintsev.tm.dto.response.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.dto.model.TaskDto;

@NoArgsConstructor
public final class TaskUnbindFromProjectResponse extends AbstractTaskResponse {

    public TaskUnbindFromProjectResponse(@Nullable final TaskDto task) {
        super(task);
    }

}
