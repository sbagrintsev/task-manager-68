package ru.tsc.sbagrintsev.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.ModelNotFoundException;
import ru.tsc.bagrintsev.tm.exception.entity.UserNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.IdIsEmptyException;
import ru.tsc.bagrintsev.tm.model.Project;
import ru.tsc.bagrintsev.tm.model.Task;
import ru.tsc.sbagrintsev.tm.marker.DBCategory;

@Category(DBCategory.class)
public final class ProjectTaskServiceTest extends AbstractTest {

    @NotNull
    private final String userId = "testUserId1";

    @Before
    public void setUp() throws ModelNotFoundException, IdIsEmptyException, UserNotFoundException {
        @NotNull final Project project = new Project();
        project.setId("projectId1");
        project.setName("projectName1");
        projectService.add(userId, project);
        @NotNull final Task task = new Task();
        task.setId("taskId1");
        task.setName("taskName1");
        taskService.add(userId, task);
    }

    @Test
    @Category(DBCategory.class)
    public void testBindTaskToProject() throws AbstractException {
        Assert.assertNull(taskService.findAll().get(0).getProject());
        projectTaskService.bindTaskToProject(userId, "projectId1", "taskId1");
        @Nullable final Project project = taskService.findOneById(userId, "taskId1").getProject();
        Assert.assertNotNull(project);
        Assert.assertEquals("projectId1", project.getId());
    }

    @Test
    @Category(DBCategory.class)
    public void testRemoveProjectById() throws AbstractException {
        Assert.assertFalse(projectService.findAll().isEmpty());
        Assert.assertFalse(taskService.findAll().isEmpty());
        projectTaskService.bindTaskToProject(userId, "projectId1", "taskId1");
        projectTaskService.removeProjectById(userId, "projectId1");
        Assert.assertTrue(projectService.findAll().isEmpty());
        Assert.assertTrue(taskService.findAll().isEmpty());
    }

    @Test
    @Category(DBCategory.class)
    public void testUnbindTaskFromProject() throws AbstractException {
        projectTaskService.bindTaskToProject(userId, "projectId1", "taskId1");
        @Nullable final Project project = taskService.findOneById(userId, "taskId1").getProject();
        Assert.assertNotNull(project);
        Assert.assertEquals("projectId1", project.getId());
        projectTaskService.unbindTaskFromProject(userId, "taskId1");
        Assert.assertNull(taskService.findOneById(userId, "taskId1").getProject());
    }

}
