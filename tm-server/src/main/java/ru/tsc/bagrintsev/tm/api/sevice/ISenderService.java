package ru.tsc.bagrintsev.tm.api.sevice;

import org.jetbrains.annotations.NotNull;

import javax.jms.JMSException;

public interface ISenderService {

    void createMessage(
            @NotNull Object object,
            @NotNull String eventType
    );

    void initJMS() throws Exception;

    void stop() throws JMSException;

    void stopJMS() throws Exception;

}
