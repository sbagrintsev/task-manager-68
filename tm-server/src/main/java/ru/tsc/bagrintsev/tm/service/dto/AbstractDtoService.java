package ru.tsc.bagrintsev.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.tsc.bagrintsev.tm.api.sevice.dto.IAbstractDtoService;
import ru.tsc.bagrintsev.tm.dto.model.AbstractDtoModel;

import java.util.Collection;
import java.util.List;

@Service
abstract class AbstractDtoService<M extends AbstractDtoModel> implements IAbstractDtoService<M> {

    @NotNull
    public abstract List<M> findAll();

    @NotNull
    public abstract Collection<M> set(@NotNull final Collection<M> records);

    public abstract long totalCount();

}
