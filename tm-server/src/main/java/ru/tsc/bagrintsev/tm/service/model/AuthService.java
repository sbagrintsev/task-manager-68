package ru.tsc.bagrintsev.tm.service.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.bagrintsev.tm.api.sevice.IPropertyService;
import ru.tsc.bagrintsev.tm.api.sevice.model.IAuthService;
import ru.tsc.bagrintsev.tm.api.sevice.model.IUserService;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.ModelNotFoundException;
import ru.tsc.bagrintsev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.IdIsEmptyException;
import ru.tsc.bagrintsev.tm.exception.user.AccessDeniedException;
import ru.tsc.bagrintsev.tm.model.Session;
import ru.tsc.bagrintsev.tm.model.User;
import ru.tsc.bagrintsev.tm.repository.model.SessionRepository;
import ru.tsc.bagrintsev.tm.util.CryptUtil;

import java.security.GeneralSecurityException;
import java.util.Date;

@Service
@RequiredArgsConstructor
public class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final SessionRepository repository;

    @NotNull
    public ApplicationContext context;

    @Override
    @Transactional
    public void clearAll() {
        repository.deleteAll();
    }

    @NotNull
    private Session createSession(@NotNull final User user) {
        @NotNull final Session session = new Session();
        session.setRole(user.getRole());
        session.setUser(user);
        repository.save(session);
        return session;
    }

    @NotNull
    public Session findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws IdIsEmptyException, ProjectNotFoundException, ModelNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (id == null || id.isEmpty()) throw new IdIsEmptyException();
        @Nullable final Session session = repository.findByUserIdAndId(userId, id);
        if (session == null) throw new ModelNotFoundException();
        return session;
    }

    @NotNull
    private String getToken(
            @NotNull final String sessionId,
            @NotNull final String userId
    ) throws JsonProcessingException, ProjectNotFoundException, IdIsEmptyException, ModelNotFoundException {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Session session = findOneById(userId, sessionId);
        @NotNull final String token = objectMapper.writeValueAsString(session);
        @NotNull final String sessionKey = propertyService.getSessionKey();
        return CryptUtil.encrypt(sessionKey, token);
    }

    @NotNull
    private String getToken(@NotNull final User user) throws JsonProcessingException, ProjectNotFoundException, IdIsEmptyException, ModelNotFoundException {
        return getToken(createSession(user).getId(), user.getId());
    }

    @NotNull
    @Override
    @Transactional
    public String signIn(
            @Nullable final String login,
            @Nullable final String password
    ) throws AbstractException, JsonProcessingException, GeneralSecurityException {
        return getToken(userService.checkUser(login, password));
    }

    @NotNull
    @Override
    public Session validateToken(@Nullable final String token) throws AccessDeniedException, JsonProcessingException {
        if (token == null) throw new AccessDeniedException();
        @NotNull final String sessionKey = propertyService.getSessionKey();
        @NotNull final String json;
        try {
            json = CryptUtil.decrypt(sessionKey, token);
        } catch (@NotNull final Exception e) {
            throw new AccessDeniedException();
        }
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Session session = objectMapper.readValue(json, Session.class);
        @NotNull final Date currentDate = new Date();
        @NotNull final Date sessionDate = session.getDate();
        final long delta = (currentDate.getTime() - sessionDate.getTime()) / 1000;
        if (delta > propertyService.getSessionTimeout()) throw new AccessDeniedException();
        return session;
    }

}
