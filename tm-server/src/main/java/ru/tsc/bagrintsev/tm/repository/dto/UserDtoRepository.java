package ru.tsc.bagrintsev.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.bagrintsev.tm.dto.model.UserDTO;
import ru.tsc.bagrintsev.tm.enumerated.Role;

@Repository
@Scope("prototype")
public interface UserDtoRepository extends AbstractDtoRepository<UserDTO> {

    void deleteByLogin(@NotNull final String login);

    boolean existsByEmail(@NotNull final String email);

    boolean existsByLogin(@NotNull final String login);

    @Nullable
    UserDTO findFirstByEmail(@NotNull final String email);

    @Nullable
    UserDTO findFirstByLogin(@NotNull final String login);

    @Modifying
    @Query(value = "UPDATE UserDTO SET role = :role WHERE login = :login")
    void setRole(
            @NotNull @Param("login") final String login,
            @NotNull @Param("role") final Role role);

    @Modifying
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Query(value = "UPDATE UserDTO SET passwordHash = :password, passwordSalt = :salt WHERE login = :login")
    void setUserPassword(
            @NotNull @Param("login") final String login,
            @NotNull @Param("password") final String password,
            final @Param("salt") byte @NotNull [] salt);

}
