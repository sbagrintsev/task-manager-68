package ru.tsc.bagrintsev.tm.repository.dto;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.bagrintsev.tm.dto.model.AbstractDtoWBSModel;

@Repository
@Scope("prototype")
public interface AbstractUserOwnedDtoRepository<M extends AbstractDtoWBSModel> extends AbstractDtoRepository<M> {

}
