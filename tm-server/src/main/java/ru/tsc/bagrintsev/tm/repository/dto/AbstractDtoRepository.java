package ru.tsc.bagrintsev.tm.repository.dto;

import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.tsc.bagrintsev.tm.dto.model.AbstractDtoModel;

@Repository
@Scope("prototype")
public interface AbstractDtoRepository<M extends AbstractDtoModel> extends JpaRepository<M, String> {

}
