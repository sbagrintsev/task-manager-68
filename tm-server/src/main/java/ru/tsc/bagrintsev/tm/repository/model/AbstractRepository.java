package ru.tsc.bagrintsev.tm.repository.model;

import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.tsc.bagrintsev.tm.model.AbstractModel;

@Repository
@Scope("prototype")
public interface AbstractRepository<M extends AbstractModel> extends JpaRepository<M, String> {

}
