package ru.tsc.bagrintsev.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.bagrintsev.tm.dto.model.TaskDto;

import java.util.List;

@Repository
@Scope("prototype")
public interface TaskDtoRepository extends AbstractUserOwnedDtoRepository<TaskDto> {

    long countByUserId(@NotNull final String userId);

    void deleteAllByUserId(@NotNull final String userId);

    void deleteByUserIdAndId(
            @NotNull final String userId,
            @NotNull final String id);

    boolean existsByUserIdAndId(
            @NotNull final String userId,
            @NotNull final String id);

    @Nullable
    List<TaskDto> findAllByUserId(@NotNull final String userId);

    @Nullable
    List<TaskDto> findAllByUserId(
            @NotNull final String userId,
            @NotNull final Sort sort);

    @Nullable
    List<TaskDto> findAllByUserIdAndProjectId(
            @NotNull final String userId,
            @NotNull final String projectId);

    @Nullable
    TaskDto findByUserIdAndId(
            @NotNull final String userId,
            @NotNull final String id);

    @Modifying
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Query(value = "UPDATE TaskDto SET projectId = :projectId WHERE userId = :userId AND id = :id")
    void setProjectId(
            @NotNull @Param("userId") final String userId,
            @NotNull @Param("id") final String id,
            @Nullable @Param("projectId") final String projectId);

    @Modifying
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Query(value = "UPDATE TaskDto SET name = :name, description = :description WHERE userId = :userId AND id = :id")
    void updateById(
            @Nullable @Param("userId") final String userId,
            @Nullable @Param("id") final String id,
            @Nullable @Param("name") final String name,
            @Nullable @Param("description") final String description);

}
