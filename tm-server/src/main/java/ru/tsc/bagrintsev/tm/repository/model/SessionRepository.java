package ru.tsc.bagrintsev.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.bagrintsev.tm.model.Session;

import java.util.List;

@Repository
@Scope("prototype")
public interface SessionRepository extends AbstractUserOwnedRepository<Session> {

    long countByUserId(@NotNull final String userId);

    void deleteAllByUserId(@NotNull final String userId);

    @Transactional
    void deleteByUserIdAndId(
            @NotNull final String userId,
            @NotNull final String id);

    boolean existsByUserIdAndId(
            @NotNull final String userId,
            @NotNull final String id);

    @Nullable
    List<Session> findAllByUserId(@NotNull final String userId);

    @Nullable
    Session findByUserIdAndId(
            @NotNull final String userId,
            @NotNull final String id);

}
